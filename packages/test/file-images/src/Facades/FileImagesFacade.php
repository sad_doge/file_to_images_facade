<?php
namespace Test\FileImages\Facades;
use Illuminate\Support\Facades\Facade;

class FileImagesFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'file-images'; }
}
