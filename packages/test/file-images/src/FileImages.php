<?php


namespace Test\FileImages;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Validator;
use Imagick;
use NcJoes\OfficeConverter\OfficeConverter;

class FileImages
{
    public static function convert($file) {

        $dir = 'documents/' . time();
        $name = time();
        $extension = $file->extension();
        $file->storeAs($dir, $name . '.' . $extension, 'public');


        if (in_array($file->extension(), ['doc', 'docx', 'ppt', 'pptx'])) {
            $converter = new OfficeConverter(Storage::disk('public')->path($dir . '/' . $name . '.' . $extension));
            $extension = 'pdf';
            $converter->convertTo(Storage::disk('public')->path($dir . '/' . $name . '.' . $extension));
        }

        $imagick = new Imagick();
        $imagick->setResolution(300, 300);
        $imagick->pingImage(Storage::disk('public')->path($dir . '/' . $name . '.' . $extension));
        $count = $imagick->getNumberImages();
        $result = [];
        for ($i = 0; $i < (min($count, 4)); $i++) {
            $imagick->readImage(Storage::disk('public')->path($dir . '/' . $name . '.' . $extension) . "[{$i}]");
            $imagick->setImageCompressionQuality(100);
            $imagick->writeImage(Storage::disk('public')->path($dir) . '/' . "page_{$i}.jpeg");

            $result[] = Storage::disk('public')->path($dir . '/' . "page_{$i}.jpeg");
        }

        return [
            'count' => $count,
            'paths' => $result
        ];
    }

}
