<?php

namespace Test\FileImages;

use Illuminate\Support\ServiceProvider;

class FileImagesServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind('file-images', function()
        {
            return new FileImages();
        });
    }
}
