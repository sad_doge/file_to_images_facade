Все упаковано в фасад
packages/test/file-images

скопировать папку packages в корневую папку проекта 

в composer.json добавить

    "repositories": {
        "test/file-images": {
            "type": "path",
            "url": "packages/test/file-images",
            "options": {
                "symlink": true
            }
        }
    },

composer install

Пример использования фасада в api.php

Выводом будет путь к файлам картинок 


требуется расширение php imagick

и след бинарники:

Ghostscript ( его юзает imagick)
https://www.ghostscript.com/releases/index.html


libreoffice (конвертирует файлы в pdf, после чего imagick pdf => jpeg)
https://www.libreoffice.org/download/download-libreoffice/

