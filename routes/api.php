<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/test-facade', function (Request $request) {
    $validator = Illuminate\Support\Facades\Validator::make($request->all(), [
        'file' => 'required|mimes:pdf,doc,docx,pptx,ppt',
    ]);

    if ($validator->fails()) {
        return response()->json($validator->errors(), 400);
    }


    $result = Test\FileImages\FileImages::convert($request->file('file'));


    return response()->json([
        'result' => $result
    ], 200);
});
